using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool instance;

    [SerializeField] private GameObject projectilePrefab;
    private Queue<GameObject> projectilePool = new Queue<GameObject>();
    [SerializeField] private int projectilePoolSize = 20;

    [SerializeField] private GameObject enemyPrefab;
    private Queue<GameObject> enemyPool = new Queue<GameObject>();
    [SerializeField] private int enemyPoolSize = 10;

    private void CheckObjectPoolInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Awake()
    {
        CheckObjectPoolInstance();
    }

    private void LoadProjectilePool()
    {
        for (int i = 0; i < projectilePoolSize; i++)
        {
            GameObject projectile = Instantiate(projectilePrefab);
            projectilePool.Enqueue(projectile);
            projectile.SetActive(false);
        }
    }

    private void LoadEnemyPool()
    {
        for (int i = 0; i < enemyPoolSize; i++)
        {
            GameObject enemy = Instantiate(enemyPrefab);
            enemyPool.Enqueue(enemy);
            enemy.SetActive(false);
        }
    }

    private void Start()
    {
        LoadProjectilePool();
        LoadEnemyPool();
    }

    public GameObject GetProjectileFromPool()
    {
        if (projectilePool.Count > 0)
        {
            GameObject projectile = projectilePool.Dequeue();
            projectile.SetActive(true);
            return projectile;
        }
        else
        {
            GameObject projectile = Instantiate(projectilePrefab);
            return projectile;
        }   
    }

    public GameObject GetEnemyFromPool()
    {
        if (enemyPool.Count > 0)
        {
            GameObject enemy = enemyPool.Dequeue();
            enemy.SetActive(true);
            return enemy;
        }
        else
        {
            GameObject enemy = Instantiate(enemyPrefab);
            return enemy;
        }
    }

    public void ReturnProjectileToPool(GameObject projectile)
    {
        projectilePool.Enqueue(projectile);
        projectile.SetActive(false);
    }

    public void ReturnEnemyToPool(GameObject enemy)
    {
        enemyPool.Enqueue(enemy);
        enemy.SetActive(false);
    }







}
    