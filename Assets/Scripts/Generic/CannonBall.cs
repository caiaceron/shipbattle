using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    [SerializeField]
    private float projectileSpeed = 250.0f;

    private Rigidbody2D rb2d;


    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rb2d.velocity = transform.up * projectileSpeed * Time.fixedDeltaTime;
    }


    private void OnBecameInvisible()
    {
        ObjectPool.instance.ReturnProjectileToPool(gameObject);
    }

}
