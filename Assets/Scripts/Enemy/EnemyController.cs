using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private Enemy enemy;
    private GameObject target;
    private AudioSource audioSource;
    private Animator animator;
    private Rigidbody2D rb2d;
    private Vector2 movement;

    private string currentAnimation;
    private WaitForSeconds waitForSeconds;
    private int shipFullHealth, shipHalfHealth, shipLowHealth, shipDestroyed;
    private float rotationAngle = 0.0f;





    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        target = GameObject.FindWithTag("Player").gameObject;

        shipFullHealth = Animator.StringToHash("Ship_Full_Health");
        shipHalfHealth = Animator.StringToHash("Ship_Half_Health");
        shipLowHealth = Animator.StringToHash("Ship_Low_Health");
        shipDestroyed = Animator.StringToHash("Ship_Destroyed");

        waitForSeconds = new WaitForSeconds(enemy.rateOfFire);

    }

    private void Start()
    {  
        StartCoroutine(EnemyMoveTowardsTarget());
    }

    private void FixedUpdate()
    {
      
    }



    IEnumerator EnemyMoveTowardsTarget()
    {  
        while (Vector2.Distance(transform.position, target.transform.position) > enemy.rangeToAttack)
        {
            Vector2 direction = target.transform.position - this.transform.position;
            rotationAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; 
            rb2d.rotation = rotationAngle;
            direction.Normalize();
            movement = direction;
            MoveEnemy(movement);
            yield return null;   
        }
        StartCoroutine(EnemyAttackTarget());
        
    }

    IEnumerator EnemyAttackTarget()
    {
        //audioSource.Play();
        Debug.Log("DIE BITCH");


        DestroyEnemy();
        yield return waitForSeconds;
        StartCoroutine(EnemyMoveTowardsTarget());
    }

    private void MoveEnemy(Vector2 direction)
    {
        rb2d.MovePosition((Vector2)transform.position + (direction * enemy.movementSpeed * Time.fixedDeltaTime));
    }

    private void ChangeAnimationState(string newState)
    {
        if (currentAnimation == newState) { return; }

        else
        {
            animator.Play(currentAnimation);
            currentAnimation = newState;
        }
    }

    private void DestroyEnemy()
    {
             
        Destroy(this.gameObject, 1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CannonBall projectile = collision.GetComponent<CannonBall>();
        if (projectile != null)
        {

            ObjectPool.instance.ReturnProjectileToPool(collision.gameObject);
            Debug.Log("Projectile HIT!!!");
        }
    }

}
