using UnityEngine;

public class EnemyStates : MonoBehaviour
{
    public enum E_ShipStates
    {
        FULL_HEALTH,
        HALF_HEALTH,
        LOW_HEALTH,
        DESTROYED
    }
}
