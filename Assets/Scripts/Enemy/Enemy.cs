using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Ship", menuName = "ShipBattle/Enemy Ship")]
public class Enemy : ScriptableObject
{
    public GameObject enemyModel;

    public int health;
    public float movementSpeed;
    public float turningSpeed;
    public float firepower;
    public float rateOfFire;
    public float rangeToAttack;


    public enum E_shipTypes
    {
        SHOOTER,
        CHASER,
        GALLEON
    }

    public E_shipTypes ShipType;
}
