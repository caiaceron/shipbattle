using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Vector2 movement;

    [SerializeField]
    private GameObject frontCannon;
    [SerializeField]
    private GameObject rightCannon;
    [SerializeField]
    private GameObject leftCannon;

    [SerializeField] 
    private int life = 100;

    [SerializeField]
    private float maxSpeed = 100.0f;

    [SerializeField] 
    private float movementSpeed = 100.0f;

    [SerializeField] 
    private float turningSpeed = 50.0f;

   
    [SerializeField][Range (0, 1)]
    private float driftFactor = 0.5f;

    [SerializeField] 
    private float dragFactor = 0.0f;

    

    [SerializeField] 
    private float attackRate = 1.0f;

    [SerializeField] 
    private float attackPower = 5.0f;


    private float rotationAngle = 0.0f;

    

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        
    }
    private void Update()
    {
        InputCalculation();
    }
    private void FixedUpdate()
    {
        SpeedCalculation();
        RotationCalculation();
        DriftFactorCorrection();
    }

    private void InputCalculation()
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.X))
        {
            SingleShotAttack();
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            LeftShotAttack();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            RightShotAttack();
        }
    }

    private void SpeedCalculation()
    {
        if (movement.y == 0)
        {
            rb2d.drag = Mathf.Lerp(rb2d.drag, dragFactor, Time.fixedDeltaTime);
        }
        else
        {
            rb2d.drag = 0.0f;
        }

        float shipVelocity = Vector2.Dot(transform.up, rb2d.velocity);
        if (shipVelocity > maxSpeed) { return; }
        if (shipVelocity < -maxSpeed * 0.5f) { return; }

        rb2d.AddForce(transform.up * movement.y * movementSpeed * Time.fixedDeltaTime, ForceMode2D.Force);

    }

    private void RotationCalculation()
    {
        rotationAngle = rotationAngle - (movement.x * turningSpeed * Time.fixedDeltaTime);
        rb2d.MoveRotation(rotationAngle);

    }

    private void DriftFactorCorrection()
    {
        Vector2 velocityUp = transform.up * Vector2.Dot(rb2d.velocity, transform.up);
        Vector2 velocityRight = transform.right * Vector2.Dot(rb2d.velocity, transform.right);
        rb2d.velocity = velocityUp + velocityRight * driftFactor;
    }

    private void SingleShotAttack()
    {
        GameObject projectile = ObjectPool.instance.GetProjectileFromPool();
        projectile.transform.position = frontCannon.transform.GetChild(0).position;
        projectile.transform.rotation = frontCannon.transform.GetChild(0).rotation;
    }

    private void LeftShotAttack()
    {
        GameObject projectile = ObjectPool.instance.GetProjectileFromPool();
        projectile.transform.position = leftCannon.transform.GetChild(0).position;
        projectile.transform.rotation = leftCannon.transform.GetChild(0).rotation;
    }

    private void RightShotAttack()
    {
        GameObject projectile = ObjectPool.instance.GetProjectileFromPool();
        projectile.transform.position = rightCannon.transform.GetChild(0).position;
        projectile.transform.rotation = rightCannon.transform.GetChild(0).rotation;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        CannonBall projectile = collision.GetComponent<CannonBall>();
        if(projectile != null)
        {
            ObjectPool.instance.ReturnProjectileToPool(collision.gameObject);
            Debug.Log("Projectile HIT!!!");
        }
    }
}
